#!/usr/bin/env node

import program, { CommandOptions } from 'commander';

program.version('1.0.0').description('Custom google cloud builder');

program
  .option('-t, --tag <image>', 'Build docker image from cache')
  .option('-d, --dockerfile <path>', 'Path to the Dockerfile')
  .action(async cmd => {
    console.log(JSON.stringify(cmd, null, 2));
  });

program.parse(process.argv);
