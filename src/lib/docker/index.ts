import Docker from 'dockerode';

export const docker = new Docker();

export * from './build-image';
export * from './pull-image';
